package template.spark

import java.io.PrintWriter

import org.apache.hadoop.conf.Configuration
import org.apache.spark.ml.classification.{MultilayerPerceptronClassificationModel, MultilayerPerceptronClassifier, RandomForestClassificationModel, RandomForestClassifier}
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature._
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.hdfs.DFSClient.Conf

final case class Tweet(id: Int, sentimentId: Int = 0, text: String)

object Main extends InitSpark {
  def main(args: Array[String]): Unit = {
    // Get the ready-to-go models
    val mpc = fitMPC()
    workCount()

    // Prepare
    val w2v = new Word2Vec()
    val assembler = new VectorAssembler().setInputCols(Array("vector")).setOutputCol("featuresRaw")
    val standardScaler = new StandardScaler().setInputCol("featuresRaw").setOutputCol("features")
    var mpcStorage = ""

    // Define streaming context
    val conf = new Configuration()
    conf.set("fs.defaultFS", "hdfs://localhost:9000/")
    val fs = FileSystem.get(conf)
    val ssc = new StreamingContext(sc, Seconds(1))
    println("SSC: " + ssc)
    val lines: ReceiverInputDStream[String] = ssc.socketTextStream("10.90.138.32", 8989)
    lines.foreachRDD { (rdd, time) =>
      // Prepare data
      val rawRDD = rdd.map(t => t.replaceAll("[^A-z\\s]", " ").toLowerCase.split(" "))

      import spark.implicits._
      val df = rawRDD.toDF("text")
      val w2vRes = w2v.fit(df).transform(df)
      var vectorized = assembler.transform(w2vRes)
      vectorized = standardScaler.fit(vectorized).transform(vectorized)

      // Run on models
      val mpcPred = mpc.transform(vectorized)

      // Get results
      val pred1 = mpcPred.select("prediction"))

      // Show it
      val entry = rdd + ", " + pred1
      println("MPC: " + rdd + ", " + pred1)

      // Collect results
      mpcStorage += time + ", " + rdd + ", " + pred1 + "\n"
    }

    // Write results
    val file1 = fs.create(new Path("aaa/mpc"))
    val writer1 = new PrintWriter(file1)
    writer1.write(mpcStorage)
    writer1.close()

    val file2 = fs.create(new Path("aaa/dtc"))
    val writer2 = new PrintWriter(file2)
    writer2.write(dtcStorage)
    writer2.close()

    // Run all
    ssc.start()
    ssc.awaitTermination()
  }

  def workCount(): Unit = {
    val dataset = spark.sparkContext.textFile("dataset/twitter/train.csv")
    val counts = dataset.flatMap(line => line.replaceAll("[^A-z\\s]", " ").split(" ")).filter(!_.isEmpty)
      .map(word => (word, 1))
      .reduceByKey(_ + _)
      .sortBy(-_._2)
    counts.take(25).foreach(println)
  }

  // OK
  def fitMPC(): MultilayerPerceptronClassificationModel = {
    import spark.implicits._

    val rawTweets = reader.csv("dataset/twitter/train.csv").as[Tweet].rdd
    val words = rawTweets.map(t => (t.text.replaceAll("[^A-z\\s]", " "), t.sentimentId))
      .map(t => (t._1.toLowerCase.split("\\s").filter(!_.isEmpty), t._2))
      .filter(t => !t._1.isEmpty)
      .toDF("text", "label")

    val w2v = new Word2Vec()
      .setInputCol("text")
      .setOutputCol("vector")
      .setVectorSize(10)
      .setMinCount(0)

    val w2vModel = w2v.fit(words)
    val w2vResult = w2vModel.transform(words)

    val assembler = new VectorAssembler().setInputCols(Array("vector")).setOutputCol("featuresRaw")
    val scaler = new StandardScaler().setInputCol("featuresRaw").setOutputCol("features")

    var vectords = assembler.transform(w2vResult)
    vectords = scaler.fit(vectords).transform(vectords)

    val vectordsSplit = vectords.randomSplit(Array[Double](0.8, 0.2))
    val train = vectordsSplit(0)
    val validate = vectordsSplit(1)
    vectordsSplit(0).take(10).foreach(println)

    val mpc = new MultilayerPerceptronClassifier()
      .setLayers(Array[Int](10, 15, 5, 2))
      .setBlockSize(128)
      .setSeed(1234L)
      .setMaxIter(100)

    val mpcModel = mpc.fit(vectords)

    val resultTrain = mpcModel.transform(train)
    val resultValidation = mpcModel.transform(validate)

    val aEvaluator = new MulticlassClassificationEvaluator().setMetricName("accuracy")
    println(s"Train set accuracy = ${aEvaluator.evaluate(resultTrain.select("prediction", "label"))}")
    println(s"Validation set accuracy = ${aEvaluator.evaluate(resultValidation.select("prediction", "label"))}")

    val fEvaluator = new MulticlassClassificationEvaluator().setMetricName("f1")
    println(s"Train set F1 = ${fEvaluator.evaluate(resultTrain.select("prediction", "label"))}")
    println(s"Validation set F1 = ${fEvaluator.evaluate(resultValidation.select("prediction", "label"))}")

    mpcModel
  }

  // OK
  def fitDTC(): RandomForestClassificationModel = {
    import spark.implicits._

    val rawTweets = reader.csv("dataset/twitter/train.csv").as[Tweet].rdd

    val words = rawTweets.map(t => (t.text.replaceAll("[^A-z\\s]", " "), t.sentimentId))
      .map(t => (t._1.toLowerCase.split("\\s").filter(!_.isEmpty), t._2))
      .filter(t => !t._1.isEmpty)
      .toDF("text", "label")

    val w2v = new Word2Vec()
      .setInputCol("text")
      .setOutputCol("vector")
      .setVectorSize(10)
      .setMinCount(0)

    val w2vModel = w2v.fit(words)
    val w2vResult = w2vModel.transform(words)

    val assembler = new VectorAssembler().setInputCols(Array("vector")).setOutputCol("featuresRaw")
    val scaler = new StandardScaler().setInputCol("featuresRaw").setOutputCol("features")

    var vectords = assembler.transform(w2vResult)
    vectords = scaler.fit(vectords).transform(vectords)

    val vectordsSplit = vectords.randomSplit(Array[Double](0.8, 0.2))
    val train = vectordsSplit(0)
    val validate = vectordsSplit(1)
    vectordsSplit(0).take(10).foreach(println)

    val randomForestClassifier = new RandomForestClassifier()
      .setImpurity("gini")
      .setMaxDepth(10)
      .setNumTrees(10)
      .setFeatureSubsetStrategy("auto")
      .setSeed(1234L)

    val randomForestModel = randomForestClassifier.fit(train)

    val resultTrain = randomForestModel.transform(train)
    val resultValidation = randomForestModel.transform(validate)

    val aEvaluator = new MulticlassClassificationEvaluator().setMetricName("accuracy")
    println(s"Train set accuracy = ${aEvaluator.evaluate(resultTrain.select("prediction", "label"))}")
    println(s"Validation set accuracy = ${aEvaluator.evaluate(resultValidation.select("prediction", "label"))}")

    val fEvaluator = new MulticlassClassificationEvaluator().setMetricName("f1")
    println(s"Train set F1 = ${fEvaluator.evaluate(resultTrain.select("prediction", "label"))}")
    println(s"Validation set F1 = ${fEvaluator.evaluate(resultValidation.select("prediction", "label"))}")

    randomForestModel
  }
}
